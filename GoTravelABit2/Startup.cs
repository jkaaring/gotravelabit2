﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GoTravelABit2.Startup))]
namespace GoTravelABit2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
} 
