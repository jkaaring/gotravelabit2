﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoTravelABit2.Models;


namespace GoTravelABit2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated) return View();
            else
            {
             ReisidEntities db = new ReisidEntities();
             Person p = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();
            if (p==null)return RedirectToAction("Create","People");
            return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


   }
}