﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoTravelABit2.Models;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;

namespace GoTravelABit2.Models
{
    partial class Person
    {
        public string FullName
        {
            get => $"{FirstName} {LastName}".Trim();
            set
            {
                if (value != "")
                {
                    FirstName = value.Split(' ')[0];
                    LastName = (value + " ").Split(' ')[1];

                }
            }
        }
        
    }
}

namespace GoTravelABit2.Controllers
{
    public class Riik
    {
        public string name { get; set; }
    }

    public class PeopleController : Controller
    {


        static List<Riik> Riigid2
        {
            get
            {
                WebRequest wr = WebRequest.Create("https://restcountries.eu/rest/v2/all");
                HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
                Stream str = resp.GetResponseStream();
                StreamReader sr = new StreamReader(str);
                string vastus = sr.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Riik>>(vastus);
            }
        }


        private ReisidEntities db = new ReisidEntities();

        // GET: People
        public ActionResult Index(int? id)
        {
           if(Request.IsAuthenticated && User.IsInRole("Administrator"))
           return View(db.People.ToList());
            return RedirectToAction("Index", "Home");
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            Person person = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();
            //Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            AspNetUser u = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();

            Person p = new Person
            {
                EMail = User.Identity.Name,
                BirthDate = u.BirthDate ?? (new System.DateTime(2000, 1, 1)),
                FirstName = u.DisplayName.Split(' ')[0],
                LastName = (u.DisplayName + " ").Split(' ')[1],
                Nationality = u.Nationality,

            };


            //ViewBag.Nationality = new SelectList(
            //CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => new { Nationality = x.EnglishName, Name = x.EnglishName }).ToList(),
            //"Nationality", "Name", person.Nationality);

            ViewBag.Nationality = new SelectList(
           Riigid2.Select(x => new { Nationality = x.name, Name = x.name }).ToList()
           , "Nationality", "Name", p.Nationality)
           ;

            return View(p);
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }


            // ViewBag.Nationality = new SelectList(
            //CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => new { Nationality = x.EnglishName, Name = x.EnglishName }).ToList(),
            //"Nationality","Name", person.Nationality);


            ViewBag.Nationality = new SelectList(
           Riigid2.Select(x => new { Nationality = x.name, Name = x.name }).ToList()
           , "Nationality", "Name", person.Nationality)
           ;

            return View(person);
        }

        
        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
