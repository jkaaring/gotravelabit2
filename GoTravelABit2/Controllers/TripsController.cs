﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoTravelABit2.Models;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web.Mvc.Html;



namespace GoTravelABit2.Models
{
    public enum TripState { Submitted = 0, Confirmed = 1, OnWay = 2, Returned = 3, Archived = 4 }

    partial class Trip
    {
        public string StateName => ((TripState)State).ToString();

        public static TripState[] StateValues => (TripState[])Enum.GetValues(typeof(TripState));

        public static Dictionary<int, string> TripState => StateValues.Select(x => new { StateNr = (int)x, StateName = x.ToString() }).ToDictionary(x => x.StateNr, x => x.StateName);

    }
}

namespace GoTravelABit2.Controllers
{
    public class TripsController : Controller
    {
        private ReisidEntities db = new ReisidEntities();


        [OutputCache(Duration = 3600, VaryByParam = "id")]
        public ActionResult Picture(int? id)
        {
            if (id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var c = db.Files.Find(id.Value);
            if (c == null) return HttpNotFound();

            return File(c.Content.ToArray(), c.ContentType);
        }


        // GET: Trips1
        public ActionResult Index(int? id, int page = 1, int pageSize = 10, string sort = "TripId", string direction = "ascending")
        {
            Person u = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();

            var trips = db.Trips.Include(t => t.Person).Where(t => t.OwnerId == u.Id);
            // var trips = u.Trips;


            var pageCount = db.Trips.Include(t => t.Person).Where(t => t.OwnerId == u.Id).Count() / pageSize + 1;


            ViewBag.Page = page;
            ViewBag.Pages = pageCount;
            ViewBag.Sort = sort;
            ViewBag.Direction = direction;
            ViewBag.PageSize = pageSize;

            trips =
                sort == "TripId" ? trips.OrderBy(x => x.Id) :
                sort == "Trip Start Date" ? trips.OrderBy(x => x.Start) :
                sort == "State" ? trips.OrderBy(x => x.State) :
                trips;


            return View(
               direction == "descending"
               ? trips.AsEnumerable().Reverse().Skip(pageSize * (page - 1)).Take(pageSize).ToList()
               : trips.Skip(pageSize * (page - 1)).Take(pageSize).ToList()
               );
        }

        // GET: Trips1/Details/5
        public ActionResult Details(int? id, int page = 1, int size = 10)
        {


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }


            var pageCount = db.TripItems.Select(t => t.TripId).Count() / size + 1;

            ViewBag.Page = page;
            ViewBag.Pages = pageCount;
            ViewBag.Size = size;
            ViewBag.Items = trip.TripItems.OrderBy(x => x.Nr).Skip((page - 1) * size).Take(size).ToList();

            return View(trip);
        }


        public ActionResult AddMember(int id, string memberEmail)
        {
            Trip trip = db.Trips.Find(id);

            Person person = db.People.Where(x => x.EMail == memberEmail).FirstOrDefault();

            if (trip == null)
            {
                return HttpNotFound();
            }

            try
            {
                trip.Attendees.Add(new Attendee { PersonId = person.Id });
                db.SaveChanges();
            }
            catch (Exception error1) when (person == null)
            {
                TempData["Message"] = $"A person with email: {memberEmail} is not GoTravelaBit user.";

                return RedirectToAction("Details", new { id = id });
            }


            try
            {
                trip.Attendees.Add(new Attendee { PersonId = person.Id });
                db.SaveChanges();
            }
            catch (Exception error2) when (db.Attendees.Where(x => x.PersonId == person.Id).FirstOrDefault() != null)
            {
                TempData["Message"] = $"A person with email: {memberEmail} is already added!";
                return RedirectToAction("Details", new { id = id });
            }

            return RedirectToAction("Details", new { id = id });
        }

        //public ActionResult RemoveMember(int id, int trip)
        //{
        //    Models.Attendee attendee = db.Attendees.Find(id);
        //    Trip t = db.Trips.Find(trip);
        //    t.Attendees.Remove(attendee);
        //    db.SaveChanges();
            
        //    return RedirectToAction("Details", new { id = trip } );
        //}

        
        // GET: Trips1/Create
        public ActionResult Create(int? id)
        {
            Person u = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();

            ViewBag.State = new SelectList(Trip.TripState, "Key", "Value", null);
            return View(new Trip { OwnerId = u.Id });
        }


        // POST: Trips1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Start,End,Name,State,Description,OwnerId")] Trip trip
                     , HttpPostedFileBase file
                    )
        {

            if (ModelState.IsValid)
            {

                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);

                        Models.File pic = new Models.File { Content = buff, OwnerId = trip.OwnerId, Name = trip.Name, ContentType = file.ContentType, FileName = file.FileName.Split('\\').Last() };

                        db.Files.Add(pic);
                        db.SaveChanges();
                        trip.Files.Add(pic);
                        db.SaveChanges();
                    }
                }

                db.Trips.Add(trip);
                db.SaveChanges();

                return RedirectToAction("Index", "Trips");
            }
            ViewBag.State = new SelectList(Trip.TripState, "Key", "Value", null);
            return View(trip);

        }

        public ActionResult MoveUp(int id)
        {
            TripItem i1 = db.TripItems.Find(id);
            TripItem i2 = db.TripItems
                .Where(x => x.TripId == i1.TripId)
                .Where(x => x.Nr < i1.Nr)
                .OrderByDescending(x => x.Nr)
                .FirstOrDefault();
            if (i2 != null)
            {
                int number = i1.Nr;
                i1.Nr = i2.Nr;
                i2.Nr = number;
                db.SaveChanges();
            }


            return RedirectToAction("Details", new { id = i1.TripId });
        }

        public ActionResult MoveDown(int id)
        {
            TripItem i4 = db.TripItems.Find(id);
            TripItem i3 = db.TripItems
                .Where(x => x.TripId >= i4.TripId)
                .Where(x => x.Nr > i4.Nr)
                .OrderBy(x => x.Nr)
                .FirstOrDefault();
            if (i3 != null)
            {
                int number1 = i4.Nr;
                i4.Nr = i3.Nr;
                i3.Nr = number1;
                db.SaveChanges();
            }

            return RedirectToAction("Details", new { id = i4.TripId });

        }

        // GET: Trips1/Edit/5
        public ActionResult Edit(int? id)

        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            ViewBag.State = new SelectList(Trip.TripState, "Key", "Value", null);
            return View(trip);
        }

        // POST: Trips1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Start,End,Name,State,Description,OwnerId")] Trip trip
            , HttpPostedFileBase file
            )

        {
            if (ModelState.IsValid)
            {
                db.Entry(trip).State = EntityState.Modified;
                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        Models.File pic = new Models.File { Content = buff, OwnerId = trip.OwnerId, Name = trip.Name, ContentType = file.ContentType, FileName = file.FileName.Split('\\').Last() };

                        db.Files.Add(pic);
                        db.SaveChanges();
                        trip.Files.Add(pic);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Details", new { id = trip.Id });
            }
            ViewBag.State = new SelectList(Trip.TripState, "Key", "Value", null);
            return View(trip);
        }

        public ActionResult DeletePicture(int id, int trip)
        {
            Models.File file = db.Files.Find(id);
            Trip t = db.Trips.Find(trip);
            t.Files.Remove(file);
            db.SaveChanges();
            if (file.Trips.Count == 0 && file.TripItems.Count == 0)
            {
                db.Files.Remove(file);
                db.SaveChanges();
            }

            return RedirectToAction("Edit", new { id = trip });

        }



        // GET: Trips1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // POST: Trips1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Trip trip = db.Trips.Find(id);
                db.Trips.Remove(trip);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception error3)
            {
                TempData["Message"] = "You have to delete all the photos before deleting a trip.";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
