﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoTravelABit2.Models;

namespace GoTravelABit2.Controllers
{
    public class ChecklistsController : Controller
    {
        private ReisidEntities db = new ReisidEntities();

        // GET: Checklists
        public ActionResult Index()
        {
            var checklists = db.Checklists.Include(c => c.Person);
            return View(checklists.ToList());
        }

        // GET: Checklists/Details/5
        public ActionResult Details(int? id, int page = 1, int size = 10)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Checklist checklist = db.Checklists.Find(id);
            if (checklist == null)
            {
                return HttpNotFound();
            }

            var pageCount = db.CheckItems.Select(t => t.ChecklistId).Count() / size + 1;

            ViewBag.Page = page;
            ViewBag.Pages = pageCount;
            ViewBag.Size = size;
            ViewBag.Items = checklist.CheckItems.OrderBy(x => x.Nr).Skip((page - 1) * size).Take(size).ToList();

            return View(checklist);
        }

        // GET: Checklists/Create
        public ActionResult Create()
        {
            Person u = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();

            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName");
            return View(new Checklist { OwnerId = u.Id });
        }

        // POST: Checklists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,OwnerId")] Checklist checklist)
        {
            if (ModelState.IsValid)
            {
                db.Checklists.Add(checklist);
                db.SaveChanges();
                return RedirectToAction("Index", "Checklists");  //siia vaja panna, et läheks CL sisse itemeid lisama
            }

            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName", checklist.OwnerId);
            return View(checklist);
        }

        // GET: Checklists/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Checklist checklist = db.Checklists.Find(id);
            if (checklist == null)
            {
                return HttpNotFound();
            }
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName", checklist.OwnerId);
            return View(checklist);
        }

        // POST: Checklists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,OwnerId")] Checklist checklist)
        {
            if (ModelState.IsValid)
            {
                db.Entry(checklist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OwnerId = new SelectList(db.People, "Id", "FirstName", checklist.OwnerId);
            return View(checklist);
        }

        // GET: Checklists/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Checklist checklist = db.Checklists.Find(id);
            if (checklist == null)
            {
                return HttpNotFound();
            }
            return View(checklist);
        }

        // POST: Checklists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Checklist checklist = db.Checklists.Find(id);
            db.Checklists.Remove(checklist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
