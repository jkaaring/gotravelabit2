﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoTravelABit2.Models;

namespace GoTravelABit2.Controllers
{
    public class FilesController : Controller
    {
        private ReisidEntities db = new ReisidEntities();

        public ActionResult Content(int? id)
        {
            if (id.HasValue)
            {
                File file = db.Files.Find(id.Value);
                if (file != null)
                {
                    return File(file.Content, file.ContentType);
                }
            }
            return HttpNotFound();

        }

        public ActionResult Index()
        {
            return View(db.Files.ToList()); 
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
