﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GoTravelABit2.Models;

namespace GoTravelABit2.Controllers
{
    public class CheckItemsController : Controller
    {
        private ReisidEntities db = new ReisidEntities();

        // GET: CheckItems
        public ActionResult Index(int? id)
        {

            Checklist checklist = db.Checklists.Find(id);
            if (checklist == null) return HttpNotFound();

            var checkItems = db.CheckItems.Include(c => c.Checklist).Where(c => c.ChecklistId == checklist.Id);
            return View(checkItems.ToList());
        }

        // GET: CheckItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckItem checkItem = db.CheckItems.Find(id);
            if (checkItem == null)
            {
                return HttpNotFound();
            }
            return View(checkItem);
        }

        // GET: CheckItems/Create
        public ActionResult Create(int id)
        {
            Checklist checklist = db.Checklists.Find(id);
            if (checklist == null) return HttpNotFound();
            int nr = (checklist.CheckItems.LastOrDefault()?.Nr ?? 0) + 1;

            ViewBag.ChecklistId = new SelectList(db.Checklists, "Id", "Name", "Nr");
            return View(new CheckItem { ChecklistId = id, Nr = nr });
        }

        // POST: CheckItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Item_Name,ChecklistId,Nr")] CheckItem checkItem)
        {
            if (ModelState.IsValid)
            {
                db.CheckItems.Add(checkItem);
                db.SaveChanges();
                return RedirectToAction("Details", "Checklists");
            }

            ViewBag.ChecklistId = new SelectList(db.Checklists, "Id", "Name", "Nr", checkItem.ChecklistId);
            return View(checkItem);
        }

        // GET: CheckItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckItem checkItem = db.CheckItems.Find(id);
            if (checkItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChecklistId = new SelectList(db.Checklists, "Id", "Name", "Nr", checkItem.ChecklistId);
            return View(checkItem);
        }

        // POST: CheckItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Item_Name,Nr,ChecklistId")] CheckItem checkItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(checkItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChecklistId = new SelectList(db.Checklists, "Id", "Name", "Nr", checkItem.ChecklistId);
            return View(checkItem);
        }

        // GET: CheckItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckItem checkItem = db.CheckItems.Find(id);
            if (checkItem == null)
            {
                return HttpNotFound();
            }
            return View(checkItem);
        }

        // POST: CheckItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CheckItem checkItem = db.CheckItems.Find(id);
            db.CheckItems.Remove(checkItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
