﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using GoTravelABit2.Models;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Linq.Expressions;


//namespace GoTravelABit2.Models
//{
//    public static class HtmlHelpers
//    {
//        public static MvcHtmlString DisplayFor<TModel, TValue>(this HtmlHelper<TModel>helper, Expression<Func<TModel, TValue>> expression)
//        {
//            StringBuilder html = new StringBuilder();
//            string disp = helper.DisplayFor(expression).ToString();
//            if ( !string.IsNullOrEmpty(disp))
//            {
//                html.AppendLine(helper.DisplayNameFor(expression).ToString());
//                html.AppendLine(disp);
//            }
//            return MvcHtmlString.Create(html.ToString());
//        }
//    }
//}

namespace GoTravelABit2.Models
{
    enum TripItemType { Travel = 1, Hotel = 2, Other = 6 }

    partial class TripItem
    {
        public string TypeName => ((TripItemType)Type).ToString();

    }
}

namespace GoTravelABit2.Models

{
    public partial class TripItem
    {

        public static Dictionary<int, string> TripItemVessels = new Dictionary<int, string>
        {
        {0, "Unknown" },
        {1, "Plane" },
        {2, "Car" },
        {3, "Ship" },
        {4, "Public Transport" },
        {5, "Other" },

        };

        public string VesselName => TripItemVessels[this.Wessel ?? 0];

    }

}

namespace GoTravelABit2.Controllers
{
    //public class Riik
    //{
    //    public string name { get; set; }
    //    //public string alpha2Code { get; set; } // saab panna riigikoodi (nt. EE)
    //}

    public class TripItemsController : Controller
    {

        //static List<string> Riigid
        //    => new List<string>

        //    { "Estonia", "Latvia", "Lithuania" };

        //    static List<Riik> Riigid2
        //{
        //    get
        //    {
        //        WebRequest wr = WebRequest.Create("https://restcountries.eu/rest/v2/all");
        //        HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
        //        Stream str = resp.GetResponseStream();
        //        StreamReader sr = new StreamReader(str);
        //        string vastus = sr.ReadToEnd();
        //        return JsonConvert.DeserializeObject<List<Riik>>(vastus);
        //    }
        //}
        private ReisidEntities db = new ReisidEntities();

        [OutputCache(Duration = 3600, VaryByParam = "id")]
        public ActionResult Pdf(int? id)
        {
            if (id == 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var c = db.Files.Find(id.Value);
            if (c == null) return HttpNotFound();

            return File(c.Content.ToArray(), c.ContentType);
        }

        public ActionResult Content(int? id)
        {
            if (id.HasValue)
            {
                Models.File file = db.Files.Find(id.Value);
                if (file != null)
                {
                    return File(file.Content, file.ContentType);
                }
            }
            return HttpNotFound();

        }


        // GET: TripItems

        public ActionResult Index(int? id)
        {

            Trip trip = db.Trips.Find(id);
            if (trip == null) return HttpNotFound();

            var tripitems = db.TripItems.Include(t => t.Trip).Where(t => t.TripId == trip.Id);

            return View(tripitems.ToList());
        }

        // GET: TripItems/Details/5
        public ActionResult Details(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripItem tripItem = db.TripItems.Find(Id);
            if (tripItem == null)
            {
                return HttpNotFound();
            }
            return View(tripItem);
        }

        //GET: TripItems/Create
        public ActionResult Create(int id, int type)
        {
            Trip trip = db.Trips.Find(id);
            if (trip == null) return HttpNotFound();
            int nr = (trip.TripItems.LastOrDefault()?.Nr ?? 0) + 1;

            ViewBag.Wessel = new SelectList(TripItem.TripItemVessels, "Key", "Value", null);

            return View(new TripItem { TripId = id, Type = type, Nr = nr });

        }


        // POST: TripItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TripId,Nr,Date,Prize,Type,Wessel,Location,ToLocation,Hotel,Duration,Name,Desscription,Comment,CheckInDate,CheckOutDate,StartTime")] TripItem tripItem
            , HttpPostedFileBase file
            )
        {
            Trip trip = db.Trips.Find(tripItem.TripId);
            bool valid = trip != null ?

                (!tripItem.CheckInDate.HasValue || !tripItem.CheckOutDate.HasValue || tripItem.CheckInDate <= tripItem.CheckOutDate)
                && 
                (!tripItem.CheckInDate.HasValue || !trip.Start.HasValue || tripItem.CheckInDate >= trip.Start) 
                && 
                (!tripItem.CheckOutDate.HasValue || !trip.End.HasValue || tripItem.CheckOutDate <= trip.End)
                &&
                (!tripItem.Date.HasValue || !trip.Start.HasValue || tripItem.Date >= trip.Start)
                &&
                (!tripItem.Date.HasValue || !trip.End.HasValue || tripItem.Date <= trip.End)

                : false;
            ViewBag.TripItemDateError = "Date(s) does not fall into trip date range";


            if (ModelState.IsValid && valid)
            {

                
                db.TripItems.Add(tripItem);
                db.SaveChanges();

                
                if (file != null && file.ContentLength > 0)
                {

                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        Models.File f = new Models.File { Content = buff, Name = tripItem.Name, ContentType = file.ContentType, FileName = file.FileName.Split('\\').Last() };

                        db.Files.Add(f);
                        db.SaveChanges();
                        tripItem.Files.Add(f);
                        db.SaveChanges();

                    }
                }

                return RedirectToAction("Details", "Trips", new { id = tripItem.TripId });
            }
          
            ViewBag.Wessel = new SelectList(TripItem.TripItemVessels, "Key", "Value", null);
            return View(tripItem);
        }



        // GET: TripItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripItem tripItem = db.TripItems.Find(id);
            if (tripItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.Wessel = new SelectList(TripItem.TripItemVessels, "Key", "Value", null);
            return View(tripItem);
        }


        // POST: TripItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TripId,Nr,Date,Prize,Type,Wessel,Location,ToLocation,Hotel,Duration,Name,Desscription,Comment,CheckInDate,CheckOutDate,StartTime")] TripItem tripItem
            , HttpPostedFileBase file
            )
        {

            Trip trip = db.Trips.Find(tripItem.TripId);
            bool valid = trip != null ?

                (!tripItem.CheckInDate.HasValue || !tripItem.CheckOutDate.HasValue || tripItem.CheckInDate <= tripItem.CheckOutDate)
                &&
                (!tripItem.CheckInDate.HasValue || !trip.Start.HasValue || tripItem.CheckInDate >= trip.Start)
                &&
                (!tripItem.CheckOutDate.HasValue || !trip.End.HasValue || tripItem.CheckOutDate <= trip.End)
                &&
                (!tripItem.Date.HasValue || !trip.Start.HasValue || tripItem.Date >= trip.Start)
                &&
                (!tripItem.Date.HasValue || !trip.End.HasValue || tripItem.Date <= trip.End)

                : false;

            ViewBag.TripItemDateError = "Date(s) does not fall into trip date range";



            if (ModelState.IsValid && valid)
            {
                db.Entry(tripItem).State = EntityState.Modified;
                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                {

                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        Models.File f = new Models.File { Content = buff, Name = tripItem.Name, ContentType = file.ContentType, FileName = file.FileName.Split('\\').Last() };

                        db.Files.Add(f);
                        db.SaveChanges();
                        tripItem.Files.Add(f);
                        db.SaveChanges();

                    }
                }

                return RedirectToAction("Details", "Trips", new { id = tripItem.TripId });
            }

            db.SaveChanges();
            return View(tripItem);


        }

        public ActionResult DeleteFile(int id, int tripitem)
        {
            Models.File file = db.Files.Find(id);
            TripItem t = db.TripItems.Find(tripitem);
            t.Files.Remove(file);
            db.SaveChanges();
            if (file.Trips.Count == 0 && file.TripItems.Count == 0)
            {
                db.Files.Remove(file);
                db.SaveChanges();
            }

            return RedirectToAction("Edit", new { id = tripitem });

        }

        // GET: TripItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripItem tripItem = db.TripItems.Find(id);
            if (tripItem == null)
            {
                return HttpNotFound();
            }
            return View(tripItem);
        }

        // POST: TripItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TripItem tripItem = db.TripItems.Find(id);
            db.TripItems.Remove(tripItem);
            db.SaveChanges();


            return RedirectToAction("Details", "Trips", new { id = tripItem.TripId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
